# Samba

A convient wrapper around https://github.com/dperson/samba.

## How to use this image

```bash
docker run -d -p 139:139 -p445:445 -v $(pwd)/share:/mnt/share wbiller/samba:latest
```

## Environment variables

| Name | Default value | Description |
|------|---------------|-------------|
|SAMBA_USER|`samba`|Name of the user to create|
|SAMBA_PASSWORD|`samba`|Password of the user to create|

## Volumes

### `/mnt/share`

Volume to hold shared files.
