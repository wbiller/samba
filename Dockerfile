FROM dperson/samba:latest

ENV SAMBA_USER samba
ENV SAMBA_PASSWORD samba

VOLUME /mnt/share

CMD /usr/bin/samba.sh -w workgroup -u "${SAMBA_USER};${SAMBA_PASSWORD}" -s "share;/mnt/share;yes;no;no;${SAMBA_USER}" -p
